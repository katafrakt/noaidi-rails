class PostsController < ApplicationController
  include Noaidi::DSL
  before_action :set_post, only: [:edit, :update, :destroy]

  # GET /posts
  # GET /posts.json
  def index
    @posts = Post.all
  end

  # GET /posts/1
  # GET /posts/1.json
  def show
    Noaidi.match find_post(params[:id]), {
      [:ok, Post] => ->(post) { render locals: { post: post } },
      [:not_found, Array] => ->(posts) { render :not_found }
    }
  end

  # GET /posts/new
  def new
    @post = Post.new
  end

  # GET /posts/1/edit
  def edit
  end

  # POST /posts
  # POST /posts.json
  def create
    @post = Post.new(post_params)

    Noaidi.match save_post(@post), {
      [:ok, Post] => ->(post) { redirect_to post, notice: 'Post successfully created' },
      [:invalid, any] => ->(validation_errors) { render :new },
      [:error, StandardError] => ->(error) { render :error_500, error: error }
    }
  end

  # PATCH/PUT /posts/1
  # PATCH/PUT /posts/1.json
  def update
    Noaidi.match save_post(@post), {
      [:ok, Post] => ->(post) { redirect_to post, notice: 'Post successfully updated' },
      [:invalid, any] => ->(validation_errors) { render :edit },
      [:error, StandardError] => ->(error) { render :error_500, error: error }
    }
  end

  # DELETE /posts/1
  # DELETE /posts/1.json
  def destroy
    @post.destroy
    respond_to do |format|
      format.html { redirect_to posts_url, notice: 'Post was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_post
      @post = Post.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def post_params
      params.require(:post).permit(:title, :content, :author)
    end

    def save_post(post)
      if !post.valid?
        return [:invalid, @post.errors]
      end

      begin
        post.save!
        [:ok, post]
      rescue StandardError => e
        [:error, e]
      end
    end

    def find_post(id)
      post = Post.find_by(id: id)
      if post
        [:ok, post]
      else
        [:not_found, other_posts(id)]
      end
    end

    def other_posts(_id)
      Post.all
    end
end
